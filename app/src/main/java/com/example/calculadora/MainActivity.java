package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    TextView txtconta, txtres;
    String result, conta;
//    ViewGroup parentButtons;
//    ViewGroup parentOperators;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtconta = (TextView) findViewById(R.id.txt_conta);
        txtres = (TextView) findViewById(R.id.txt_res);
//        parentButtons = (ViewGroup) findViewById(R.id.parentButtons);
//        insertNum(parentButtons);
//        parentOperators = (ViewGroup) findViewById(R.id.parentOperators);
//        insertOp(parentOperators);

    }
    public void Button_1 (View view){
        txtres.append("1");
    }
    public void Button_2 (View view){
        txtres.append("2");
    }
    public void Button_3 (View view){
        txtres.append("3");
    }
    public void Button_4 (View view){
        txtres.append("4");
    }
    public void Button_5 (View view){
        txtres.append("5");
    }
    public void Button_6 (View view){
        txtres.append("6");
    }
    public void Button_7 (View view){
        txtres.append("7");
    }
    public void Button_8 (View view){
        txtres.append("8");
    }
    public void Button_9 (View view){
        txtres.append("9");
    }
    public void Button_0 (View view){
        txtres.append("0");
    }
    public void virgula (View view){
        result = txtres.getText().toString();

        if(!result.isEmpty()){
            if(result.charAt(result.length()-1) != ',' && !result.contains(",")) {
                txtres.append(",");
            }else{
                Toast.makeText(MainActivity.this, "Operação inválida!", Toast.LENGTH_SHORT).show();
            }
        }else{
            txtres.append("0,");
        }
    }
    public void igual (View view){
        result = txtres.getText().toString();

        if(!txtconta.getText().toString().isEmpty() && !txtres.getText().toString().isEmpty()) {
            txtconta.append(" " + txtres.getText() + " ");
            String conta = txtconta.getText().toString();
            txtconta.setText("");
            conta = conta.replace(',', '.');

            List<String> contaSep = Arrays.asList(conta.split(" "));
            ArrayList<Double> numeros = new ArrayList<>();
            ArrayList<Character> operacoes = new ArrayList<>();

            for (int i = 1; i < contaSep.size(); i++) {
                if (contaSep.get(i).equals("+")) {
                    numeros.add(Double.parseDouble(contaSep.get(i - 1)));
                    operacoes.add('+');
                } else if (contaSep.get(i).equals("-")) {
                    numeros.add(Double.parseDouble(contaSep.get(i - 1)));
                    operacoes.add('-');
                } else if (contaSep.get(i).equals("x")) {
                    numeros.add(Double.parseDouble(contaSep.get(i - 1)));
                    operacoes.add('x');
                } else if (contaSep.get(i).equals("/")) {
                    if (contaSep.get(i+1).equals("0")){
                        Toast.makeText(this, "Operação inválida!", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        numeros.add(Double.parseDouble(contaSep.get(i - 1)));
                        operacoes.add('/');
                    }
                }
            }

            numeros.add(Double.parseDouble(contaSep.get(contaSep.size() - 1)));

            ArrayList<Double> numeros2 = new ArrayList<>();
            ArrayList<Character> operacoes2 = new ArrayList<>();
            Double num = numeros.get(0);
            int comeco = 0;

            for (int i = 0; i < operacoes.size(); i++) {
                if (operacoes.get(i).equals('+') || operacoes.get(i).equals('-')) {

                    for (int j = comeco; j < i; j++) {
                        if (operacoes.get(j).equals('x')) {
                            num *= numeros.get(j + 1);
                        } else {
                            num /= numeros.get(j + 1);
                        }
                    }

                    numeros2.add(num);
                    comeco = i + 1;
                    num = numeros.get(i + 1);

                    if (operacoes.get(i).equals('+')) {
                        operacoes2.add('+');
                    } else {
                        operacoes2.add('-');
                    }
                }
            }

            for (int i = comeco; i < operacoes.size(); i++) {
                if (operacoes.get(i).equals('x')) {
                    num *= numeros.get(i + 1);
                } else {
                    num /= numeros.get(i + 1);
                }
            }
            numeros2.add(num);

            num = numeros2.get(0);
            for (int i = 0; i < operacoes2.size(); i++) {
                if (operacoes2.get(i).equals('+')) {
                    num += numeros2.get(i + 1);
                } else {
                    num -= numeros2.get(i + 1);
                }
            }

            DecimalFormat df = new DecimalFormat("##.##");
            df.setRoundingMode(RoundingMode.DOWN);

            txtres.setText(df.format(num).replace('.',','));
        }
    }
    public void apagar (View view){
        txtconta.setText("");
        txtres.setText("");
    }
    //====================================================================================================================================


//    public void insertNum(ViewGroup parent){
//        int countChild = parent.getChildCount();
//
//        for (int i = 0; i< countChild; i++){
//            final View child = parent.getChildAt(i);
//
//            ((Button)child).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(txtres.getText().equals("0")){
//                        txtres.setText("");
//                    }
//                    String testr = ((Button) child).getText().toString();
//                    if(testr.equals("=")){
//                        igual();
//                    }if(testr.equals(",")){
//                        virgula();
//                    }else {
//                        Toast.makeText(MainActivity.this, "Op: " + testr, Toast.LENGTH_SHORT).show();
//
//                        String concat = txtres.getText().toString();
//                        String tests = concat += testr;
//                        txtres.setText(tests);
//                    }
//                }
//            });
//        }
//
//    }


    //==============================================================================================


//    public void insertOp(ViewGroup parent){
//        int countChild = parent.getChildCount();
//
//        for (int i = 0; i < countChild; i++){
//            final View child = parent.getChildAt(i);
//
//            ((Button)child).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    int total = txtconta.getText().toString().length();
//                    Toast.makeText(MainActivity.this, "Total: " + total, Toast.LENGTH_SHORT);
//                    if(total > 3){
//                        String pegafinal = txtconta.getText().toString().substring(total -2, total-1);
//
//                    }else{
//                        txtconta.append(txtres.getText() + " + ");
//                        txtres.setText("0");
//                    }
//
//                    if(txtconta.getText().toString().endsWith(((Button) child).getText().toString())){
//
//                    }else if(txtconta.getText().toString().endsWith(" + ")){
//
//                    }
//                }
//            });
//        }
//    }


    //==============================================================================================
    public void soma (View view){
        result = txtres.getText().toString();
        conta = txtconta.getText().toString();

        if (result.isEmpty() && !conta.isEmpty()){
            txtconta.setText(conta.substring(0, conta.length()-2) + " +");
        }else if(!result.isEmpty() && result.charAt(result.length()-1) != ','){
            txtconta.append(" " + txtres.getText() + " +");
            txtres.setText("");
        }
    }
    public void subt (View view){
        result = txtres.getText().toString();
        conta = txtconta.getText().toString();

        if (result.isEmpty() && !conta.isEmpty()){
            txtconta.setText(conta.substring(0, conta.length()-2) + " -");
        }else if(!result.isEmpty() && result.charAt(result.length()-1) != ',') {
            txtconta.append(" " + txtres.getText() + " -");
            txtres.setText("");
        }
    }
    public void mult (View view){
        result = txtres.getText().toString();
        conta = txtconta.getText().toString();

        if (result.isEmpty() && !conta.isEmpty()){
            txtconta.setText(conta.substring(0, conta.length()-2) + " x");
        }else if(!result.isEmpty() && result.charAt(result.length()-1) != ',') {
            txtconta.append(" " + txtres.getText() + " x");
            txtres.setText("");
        }
    }
    public void div (View view){
        result = txtres.getText().toString();
        conta = txtconta.getText().toString();

        if (result.isEmpty() && !conta.isEmpty()){
            txtconta.setText(conta.substring(0, conta.length()-2) + " /");
        }else if(!result.isEmpty() && result.charAt(result.length()-1) != ',') {
            txtconta.append(" " + txtres.getText() + " /");
            txtres.setText("");
        }
    }
}
